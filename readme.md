# Movie App

## Published Online
APK: [Link](https://drive.google.com/file/d/1kB-U59kCQ9co4C5z1Os9kddfro3p0KJ6/view?usp=sharing ) 
Video: [Link](https://www.youtube.com/watch?v=vjbOILINdL0)

## Short description
Movie App provides following functionality:
- App is available in light and dark mode
- View popular, upcoming and top rated movies (with paging on scroll)
- Search movies and people (cast)
- View movie detail (poster image or youtube trailer, rating, cast, runtime, release date)
- Show person movies
- Create own movie collection (a single collection can contain at most 20 movies)
- Import/Share collection via QR code

## Technologies && libraries
- Fragment navigation (using Navigation Component)
- API requests (using RetroFit) ('https://developers.themoviedb.org/3/getting-started/introduction')
- Movie collections stored in local storage (Room DB)
- Remote image loading using Glide
- Movie/Person/Collection lists using RecyclerView
- Collection edit Tab Layout using ViewPager2
- MVVM Architecture
- One "dangerous" permission for Camera (used for QR code scanner)
- Firebase Analytics (logging the most viewed movies) & Crashlytics

## App is based on this Figma design
https://www.figma.com/file/CpnhH9YXedfAxLmd6h71E8/Material-3-Design-Kit-(Community)?node-id=51195%3A4667



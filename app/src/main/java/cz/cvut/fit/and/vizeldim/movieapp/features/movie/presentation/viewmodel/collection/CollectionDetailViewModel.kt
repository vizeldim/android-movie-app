package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.launch


class CollectionDetailViewModel(
    private val repository: IMovieRepository
) : ViewModel() {
    private val _collectionDetailStateStream: MutableLiveData<CollectionDetailState>
        = MutableLiveData(CollectionDetailState())

    val collectionDetailStateStream: LiveData<CollectionDetailState>
        get() = _collectionDetailStateStream


    fun loadMovies(collectionId: Long) {
        viewModelScope.launch {
            repository.streamMoviesByCollectionId(collectionId).collect {
                _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                    movies = it,
                    moviesCnt = it.size
                )
            }
        }
    }

    fun addMovie(collectionId: Long, movie: SimpleMovie) {
        val cnt = _collectionDetailStateStream.value?.moviesCnt
        cnt?.let {
            if(it >= 20) {
                _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                    notEnoughSpace = true
                )
                return
            }
        }
        viewModelScope.launch {
            try {
                repository.addMovieToCollection(collectionId, movie)

                _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                    successAdd = true,
                    movies = emptyList()
                )
            } catch (e: Throwable) {
                _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                    error = true
                )
            }
        }
    }


    fun removeMovie(collectionId: Long, movie: SimpleMovie) {
        viewModelScope.launch {
            repository.removeMovieFromCollection(collectionId, movie)
        }
    }


    fun changeName(collectionId: Long, name: String) {
        if(name == _collectionDetailStateStream.value?.collectionName) {
            return
        }

        viewModelScope.launch {
            val success: Boolean = repository.updateCollectionName(collectionId, name)

            _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                collectionName = if(success) name else "",
                successSave = success
            )
        }
    }

    fun hideError() {
        _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
            error = false
        )
    }

    fun hideSuccessSave() {
        _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
            successSave = false
        )
    }

    fun hideSuccessAdd() {
        _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
            successAdd = false
        )
    }

    fun hideSuccessNotEnoughSpace() {
        _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
            notEnoughSpace = false
        )
    }

    fun hideSuccessRemove() {
        _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
            successRemove = false
        )
    }


    fun loadCollectionInfo(collectionId: Long) {
        viewModelScope.launch {
            repository.streamCollectionById(collectionId).collect {
                _collectionDetailStateStream.value = _collectionDetailStateStream.value?.copy(
                    collectionName = it.name
                )
            }
        }
    }
}


data class CollectionDetailState(
    val movies: List<SimpleMovie> = emptyList(),
    val moviesCnt: Int = 0,
    val notEnoughSpace: Boolean = false,
    val collectionName: String = "",
    val loading: Boolean = false,
    val error: Boolean = false,
    val totalAdded: Int = 0,
    val successSave: Boolean = false,
    val successAdd: Boolean = false,
    val successRemove: Boolean = false
)
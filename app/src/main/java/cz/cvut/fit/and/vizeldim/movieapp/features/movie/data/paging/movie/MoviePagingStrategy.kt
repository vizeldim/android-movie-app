package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import java.io.Serializable

interface MoviePagingStrategy : Serializable {
    suspend fun getMoviePage(movieRepository: IMovieRepository, page: Int): List<Movie>
}

class PopularMoviePagingStrategy : MoviePagingStrategy {
    override suspend fun getMoviePage(movieRepository: IMovieRepository, page: Int): List<Movie> =
        movieRepository.getPopularPage(page)
}

class UpcomingMoviePagingStrategy : MoviePagingStrategy {
    override suspend fun getMoviePage(movieRepository: IMovieRepository, page: Int): List<Movie> =
        movieRepository.getPopularPage(page)
}

class TopRatedMoviePagingStrategy : MoviePagingStrategy {
    override suspend fun getMoviePage(movieRepository: IMovieRepository, page: Int): List<Movie> =
        movieRepository.getPopularPage(page)
}


class PersonMoviePagingStrategy(private val personId: Long) : MoviePagingStrategy {
    override suspend fun getMoviePage(movieRepository: IMovieRepository, page: Int): List<Movie> =
        movieRepository.getPageByPersonId(personId, page)
}


package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemSearchPersonBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person


class PersonPagingAdapter(
    private val onPersonClick: (Person) -> Unit
) : PagingDataAdapter<Person, PersonPagingAdapter.PersonHolder>(PersonAdapter.PersonDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): PersonHolder {
        val binding = ItemSearchPersonBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return PersonHolder(binding, onPersonClick)
    }

    override fun onBindViewHolder(movieHolder: PersonHolder, position: Int) {
        val person = getItem(position)
        movieHolder.bind(person)
    }

    class PersonHolder(
        private val binding: ItemSearchPersonBinding,
        private val onPersonClick: (Person) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(person: Person?) {
            person?.let {
                if (it.img != null) {
                    Glide.with(itemView).load("https://image.tmdb.org/t/p/w200${it.img}")
                        .into(binding.img)
                } else {
                    binding.img.setImageResource(R.drawable.avatar_blank_2)
                }
                binding.txtName.text = it.name
                binding.txtKnownFor.text = it.knownForDepartment
                val p = it
                binding.root.setOnClickListener { onPersonClick(p) }
            }
        }
    }
}

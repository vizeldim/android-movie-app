package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.flow.Flow

interface IMovieRepository {
    suspend fun getPopularPage(page: Int = 1): List<Movie>

    suspend fun getTopRatedPage(page: Int = 1): List<Movie>

    suspend fun getUpcomingPage(page: Int = 1): List<Movie>

    suspend fun getPageByName(
        name: String,
        page: Int = 1
    ): List<Movie>

    suspend fun getById(id: Long): Movie

    suspend fun getPopularPersonPage(page: Int = 1): List<Person>

    suspend fun getPersonPageByName(
        name: String,
        page: Int = 1
    ): List<Person>

    suspend fun getPageByPersonId(
        id: Long,
        page: Int = 1
    ): List<Movie>

    fun streamCollections(): Flow<List<MovieCollection>>

    suspend fun addCollection(name: String): Long

    suspend fun addCollection(name: String, movies: List<SimpleMovie>): Long

    fun streamMoviesByCollectionId(collectionId: Long): Flow<List<SimpleMovie>>

    suspend fun addMovieToCollection(collectionId: Long, movie: SimpleMovie): Long

    suspend fun removeMovieFromCollection(collectionId: Long, movie: SimpleMovie): Boolean

    suspend fun updateCollectionName(collectionId: Long, name: String): Boolean

    fun streamCollectionById(collectionId: Long): Flow<MovieCollection>
}
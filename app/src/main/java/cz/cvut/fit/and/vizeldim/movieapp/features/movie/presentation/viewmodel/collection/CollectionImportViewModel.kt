package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.*
import kotlinx.coroutines.launch

class CollectionImportViewModel (
    private val repository: IMovieRepository,
    private val codeParser: CodeContentParserInterface
) : ViewModel() {
    private val _collectionImportStateStream: MutableLiveData<CollectionImportState>
            = MutableLiveData(CollectionImportState())

    val collectionImportViewModel: LiveData<CollectionImportState>
        get() = _collectionImportStateStream


    fun scan(codeScanner: CodeScannerInterface) {
        _collectionImportStateStream.value = _collectionImportStateStream.value?.copy(loading = true)

        viewModelScope.launch {
            val codeContentStr: String = codeScanner.scan()

            try {
                val codeContent: CodeContent? = codeParser.parse(codeContentStr)

                codeContent?.let {
                    val collectionId: Long = repository.addCollection(it.collectionName, it.movies)

                    val movieCollection = MovieCollection(collectionId, codeContent.collectionName)

                    _collectionImportStateStream.value =
                        _collectionImportStateStream.value?.copy(movieCollection = movieCollection, loading = true)

                }
            } catch (e: CodeContentParseException) {
                _collectionImportStateStream.value =
                    _collectionImportStateStream.value?.copy(movieCollection = null, error = true)

            }
        }
    }

    fun hideError() {
        _collectionImportStateStream.value =
            _collectionImportStateStream.value?.copy(error = false)

    }
}


data class CollectionImportState(
    val movieCollection: MovieCollection? = null,
    val loading: Boolean = false,
    val error: Boolean = false
)


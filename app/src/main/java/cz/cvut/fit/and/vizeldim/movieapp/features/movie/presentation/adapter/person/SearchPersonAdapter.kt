package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemSearchPersonBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person


class SearchPersonAdapter(
    private val onPersonClick: (Person) -> Unit
) : ListAdapter<Person, PersonPagingAdapter.PersonHolder>(PersonAdapter.PersonDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): PersonPagingAdapter.PersonHolder {
        val binding = ItemSearchPersonBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return PersonPagingAdapter.PersonHolder(binding, onPersonClick)
    }


    override fun onBindViewHolder(personHolder: PersonPagingAdapter.PersonHolder, position: Int) {
        val person = getItem(position)
        personHolder.bind(person)
    }
}

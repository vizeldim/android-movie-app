package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.APagingSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie

class MoviePagingSource(
    private val repository: IMovieRepository,
    private val pagingStrategy: MoviePagingStrategy
) : APagingSource<Movie>() {

    override suspend fun getPageData(page: Int): List<Movie> =
        pagingStrategy.getMoviePage(repository, page)
}
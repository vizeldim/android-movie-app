package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import kotlinx.coroutines.launch

class CollectionsViewModel(
    private val repository: IMovieRepository
) : ViewModel() {
    private val _collectionsStateStream: MutableLiveData<CollectionsState>
        = MutableLiveData(CollectionsState())

    val collectionsStateStream: LiveData<CollectionsState>
        get() = _collectionsStateStream

    init {
        loadCollections()
    }

    private fun loadCollections() {
        viewModelScope.launch {
            repository.streamCollections().collect {
                _collectionsStateStream.value = _collectionsStateStream.value?.copy(
                    collections = it
                )
            }
        }
    }

    fun addCollection(name: String) {
        viewModelScope.launch {
            repository.addCollection(name)
        }
    }
}


data class CollectionsState(
    val collections: List<MovieCollection> = emptyList(),
    val loading: Boolean = false,
    val error: Boolean = false
)
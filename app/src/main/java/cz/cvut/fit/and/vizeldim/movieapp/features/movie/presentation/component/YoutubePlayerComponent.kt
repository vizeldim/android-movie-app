package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.component

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.DefaultPlayerUiController

class YoutubePlayerComponent(private val youTubePlayerView: YouTubePlayerView) {
    init {
        val listener: YouTubePlayerListener = object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                val defaultPlayerUiController =
                    DefaultPlayerUiController(youTubePlayerView, youTubePlayer)
                youTubePlayerView.setCustomPlayerUi(defaultPlayerUiController.rootView)
            }
        }


        val options: IFramePlayerOptions = IFramePlayerOptions.Builder().controls(0).build()

        youTubePlayerView.initialize(listener, options)
    }

    fun enterFullScreen() {
        youTubePlayerView.enterFullScreen()
    }

    fun exitFullScreen() {
        youTubePlayerView.exitFullScreen()
    }

    fun cueVideo(videoKey: String) {
        youTubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                youTubePlayer.cueVideo(videoKey, 0f)
            }
        })
    }
}
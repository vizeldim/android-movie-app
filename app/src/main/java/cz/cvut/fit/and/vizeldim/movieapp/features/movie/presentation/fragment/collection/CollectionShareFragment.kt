package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.collection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentCollectionShareBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionShareViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CollectionShareFragment : Fragment() {
    private var _binding: FragmentCollectionShareBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val args: CollectionShareFragmentArgs by navArgs()

    private val viewModel by viewModel<CollectionShareViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCollectionShareBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loader.isVisible = true

        viewModel.collectionShareStateStream.observe(viewLifecycleOwner) {
                state ->
            if(state != null) {
                binding.loader.isVisible = state.loading

                if(state.error) {
                    handleError()
                    viewModel.hideException()
                } else if(state.code != null){
                    binding.imgQrCode.setImageBitmap(state.code)
                }
            }
        }

        val mc = MovieCollection(args.collectionId, args.collectionName)
        viewModel.loadCode(mc)
    }

    private fun handleError() {
        Toast.makeText(context, "code generator error", Toast.LENGTH_LONG).show()
    }

}
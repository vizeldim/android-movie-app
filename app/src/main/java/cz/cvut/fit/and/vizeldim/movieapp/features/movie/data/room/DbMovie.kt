package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class DbMovie(
    @PrimaryKey
    val movieId: Long,
    val title: String,
    val img: String?
)
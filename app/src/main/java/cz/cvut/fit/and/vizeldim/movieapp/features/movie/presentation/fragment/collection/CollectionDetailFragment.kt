package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.collection

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.DialogAddCollectionBinding
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentCollectionDetailBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.SimpleMovieAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseCollectionDetailFragment: Fragment() {
    private var _binding: FragmentCollectionDetailBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    protected val args: CollectionDetailFragmentArgs by navArgs()

    protected var mCollectionName: String = ""

    protected open val adapter = SimpleMovieAdapter(::navigateToDetail)

    protected val viewModel by viewModel<CollectionDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCollectionDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loadMovies(args.collectionId)
        viewModel.loadCollectionInfo(args.collectionId)

        binding.list.layoutManager = FlexboxLayoutManager(context)
        binding.list.adapter = adapter

        mCollectionName = args.collectionName

        createViewModelObserver()
    }

    abstract fun createViewModelObserver()


    private fun navigateToDetail(movie: SimpleMovie) {
        val directions = CollectionDetailFragmentDirections
            .actionToMovieDetail(movie.id, movie.title)
        findNavController().navigate(directions)
    }

}

class CollectionDetailFragment : BaseCollectionDetailFragment() {
    private val collectionEditDialog: AlertDialog by lazy {
        val dialogBinding = DialogAddCollectionBinding.inflate(LayoutInflater.from(requireContext()))
        val dialogBuilder = MaterialAlertDialogBuilder(requireContext())
        val nameInput = dialogBinding.plainTextInput

        dialogBuilder.setView(dialogBinding.root)
            .setTitle("Edit Movie Collection")
            .setMessage("Enter new name")
            .setPositiveButton("Save") { dialog, _ ->
                val nameStr = nameInput.text.toString()

                if(nameStr.isEmpty()) {
                    Snackbar.make(requireView(), "Invalid Name.", 1000).show()
                } else {
                    viewModel.changeName(args.collectionId, nameStr)
                    nameInput.setText("")
                    dialog.dismiss()
                }
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                nameInput.setText("")
                dialog.dismiss()
            }
            .create()
    }

    override fun createViewModelObserver() {
        viewModel.collectionDetailStateStream.observe(viewLifecycleOwner) {
                state -> state?.apply {
                    if(collectionName.isNotEmpty() && mCollectionName != collectionName) {
                        handleCollectionNameChange(collectionName)
                    }
                    adapter.submitList(movies)
                }
        }
    }

    private fun handleCollectionNameChange(collectionName: String) {
        viewModel.hideSuccessSave()

        mCollectionName = collectionName
        (requireActivity() as? AppCompatActivity)?.supportActionBar?.title =
            "Movies - $collectionName"
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_collection_detail, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.menu_item_collection_change_name -> {
                collectionEditDialog.show()
                true
            }
            R.id.menu_item_collection_edit -> {
                navigateToEdit()
                true
            }
            R.id.menu_item_collection_share -> {
                navigateToShare()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun navigateToEdit() {
        val directions = CollectionDetailFragmentDirections
            .actionToCollectionEdit(args.collectionId, args.collectionName)

        findNavController().navigate(directions)
    }

    private fun navigateToShare() {
        val directions = CollectionDetailFragmentDirections
            .actionToCollectionShare(args.collectionId, mCollectionName)

        findNavController().navigate(directions)
    }

}
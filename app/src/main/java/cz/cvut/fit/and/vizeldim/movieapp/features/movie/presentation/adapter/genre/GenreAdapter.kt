package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.genre

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemGenreBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Genre


class GenreAdapter : ListAdapter<Genre, GenreAdapter.GenreHolder>(GenreDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GenreHolder {
        val binding = ItemGenreBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return GenreHolder(binding)
    }

    override fun onBindViewHolder(genreHolder: GenreHolder, position: Int) {
        val genre = getItem(position)
        genreHolder.bind(genre)
    }

    class GenreHolder(private val binding: ItemGenreBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(genre: Genre) {
            binding.txtName.text = genre.name
        }
    }

    private class GenreDiffCallback : DiffUtil.ItemCallback<Genre>() {

        override fun areItemsTheSame(
            oldItem: Genre,
            newItem: Genre
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Genre,
            newItem: Genre
        ): Boolean {
            return oldItem == newItem
        }
    }
}

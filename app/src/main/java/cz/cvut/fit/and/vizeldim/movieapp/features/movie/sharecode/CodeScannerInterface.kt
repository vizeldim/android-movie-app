package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode

interface CodeScannerInterface {
    suspend fun scan(): String
}
package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.sharecode.CodeRenderer
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.sharecode.qr.SquareQrCodeRenderer
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.*
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr.ZxingQrCodeGenerator
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CollectionShareViewModel (
    private val repository: IMovieRepository,
    private val codeParser: CodeContentParserInterface,
    private val codeGenerator: CodeGeneratorInterface = ZxingQrCodeGenerator(),
    private val codeRenderer: CodeRenderer = SquareQrCodeRenderer()
) : ViewModel() {
    private val _collectionShareStateStream: MutableLiveData<CollectionShareState>
            = MutableLiveData(CollectionShareState())

    val collectionShareStateStream: LiveData<CollectionShareState>
        get() = _collectionShareStateStream


    fun loadCode(collection: MovieCollection) {
        _collectionShareStateStream.value = _collectionShareStateStream.value?.copy(loading = true)

        viewModelScope.launch {
            repository.streamMoviesByCollectionId(collection.id).collect {
                val codeContentStr = codeParser
                    .stringify(
                        CodeContent(
                            collectionName = collection.name,
                            movies = it
                        )
                    )

                try {
                    val code = codeGenerator.generate(codeContentStr)
                    val bmp =  withContext(Dispatchers.IO) {
                        codeRenderer.render(code)
                    }

                    _collectionShareStateStream.value =
                        _collectionShareStateStream.value?.copy(code = bmp)
                } catch (e: CodeGeneratorException) {
                    _collectionShareStateStream.value =
                        _collectionShareStateStream.value?.copy(error = true)
                } finally {
                    _collectionShareStateStream.value =
                        _collectionShareStateStream.value?.copy(loading = false)
                }
            }
        }
    }


    fun hideException() {
        _collectionShareStateStream.value =
            _collectionShareStateStream.value?.copy(error = false)
    }
}


data class CollectionShareState(
    val code: Bitmap? = null,
    val loading: Boolean = false,
    val error: Boolean = false
)


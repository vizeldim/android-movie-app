package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class SimpleMovie(val id: Long, val title: String, val img: String?)

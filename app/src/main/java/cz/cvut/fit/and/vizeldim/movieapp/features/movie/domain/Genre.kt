package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class Genre(
    val id: Int,
    val name: String
)
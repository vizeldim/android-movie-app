package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.movie

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentSearchBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.SearchMovieAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.SearchMovieViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseMovieSearchFragment : Fragment() {
    private var _binding: FragmentSearchBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    protected val binding get() = _binding!!

    private lateinit var searchView: SearchView

    protected abstract val adapter: SearchMovieAdapter

    protected val viewModel by viewModel<SearchMovieViewModel>()

    protected open fun afterBinding() {}

    abstract val myViewLifecycleOwner: LifecycleOwner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        afterBinding()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.searchCharacters.layoutManager = LinearLayoutManager(context)
        binding.searchCharacters.adapter = adapter

        viewModel.searchStateStream.observe(myViewLifecycleOwner) { state ->
            if (state != null) {
                binding.searchProgressBar.isVisible = state.isLoading

                if (state.showError) {
                    Snackbar.make(binding.root, "Not Found", Snackbar.LENGTH_SHORT).show()
                    viewModel.hideError()
                } else {
                    adapter.submitList(state.foundMovies)
                }
            }
        }

        additionalViewModelObserver()
    }

    protected open fun additionalViewModelObserver() {}

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)

        searchView = menu.findItem(R.id.search)?.actionView as SearchView

        searchView.isIconified = false
        searchView.requestFocusFromTouch()

        searchView.setOnCloseListener {
            viewModel.clearFoundResults()

            searchView.setQuery("", false)
            true
        }

        val queryChangeListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search(query)
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                search(query)
                return true
            }
        }

        searchView.setOnQueryTextListener(queryChangeListener)
    }

    protected open fun search(query: String?) {
        query?.let { viewModel.searchByName(it) }
    }

    protected fun navigateToDetail(movie: Movie) {
        val directions =
            MovieSearchFragmentDirections.actionToDetail(
                movie.id.toLong(),
                movie.title
            )
        findNavController().navigate(directions)
    }

    override fun onPause() {
        super.onPause()
        searchView.clearFocus()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        searchView.clearFocus()
    }
}

class MovieSearchFragment : BaseMovieSearchFragment() {
    override val adapter: SearchMovieAdapter = SearchMovieAdapter(::navigateToDetail)

    override val myViewLifecycleOwner
        get() = viewLifecycleOwner
}
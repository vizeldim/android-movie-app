package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.collection

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ActivityScannerBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionImportViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeScannerInterface
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr.BudiyevCodeScanner
import org.koin.androidx.viewmodel.ext.android.viewModel

class CollectionImportFragment: Fragment() {
    private var _binding: ActivityScannerBinding? = null

    private val binding: ActivityScannerBinding
        get() = _binding!!

    private val viewModel by viewModel<CollectionImportViewModel>()

    private var _scanner: CodeScannerInterface? = null

    private val scanner: CodeScannerInterface
        get() = _scanner!!

    private val permissionsLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            permissions -> if(permissions[Manifest.permission.CAMERA] == true) { startScanner() }
    else { handleCameraPermissionDenied() }
    }

    private fun handleCameraPermissionDenied() {
        Toast.makeText(context, "Camera is required to scan QR code", Toast.LENGTH_LONG).show()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityScannerBinding.inflate(inflater)

        initScanner()
        startScannerWithPermission()

        return binding.root
    }

    private fun startScannerWithPermission() {
        permissionsLauncher.launch(arrayOf(Manifest.permission.CAMERA))
    }

    private fun initScanner() {
        _scanner = BudiyevCodeScanner(requireContext(), binding.qrScanner)

        viewModel.collectionImportViewModel.observe(viewLifecycleOwner) {
                state ->
            if(state != null) {
                if(state.error) {
                    viewModel.hideError()
                    handleError()
                }
                else {
                    state.movieCollection?.let {
                        navigateToCollection(movieCollection = it)
                    }
                }
            }
        }
    }

    private fun handleError() {
        Toast.makeText(context, "Invalid QR code", Toast.LENGTH_LONG).show()
    }

    private fun navigateToCollection(movieCollection: MovieCollection) {
        val directions = CollectionImportFragmentDirections
            .actionToCollectionDetail(movieCollection.id, movieCollection.name)

        findNavController().navigate(directions)
    }

    private fun startScanner() {
        if(isCameraPermissionGranted) {
            viewModel.scan(scanner)
        } else {
            handleCameraPermissionDenied()
        }
    }

    private val isCameraPermissionGranted: Boolean
        get() {
            return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) ==
                    PackageManager.PERMISSION_GRANTED
        }
}

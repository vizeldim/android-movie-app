package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeContent
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeContentParseException
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeContentParserInterface
import java.io.IOException

class JsonCodeContentParser : CodeContentParserInterface {
    companion object {
        private val moshi = Moshi.Builder().build()

        private val converter: JsonAdapter<CodeContentSerializable> =
            moshi.adapter(CodeContentSerializable::class.java)

        @JsonClass(generateAdapter = true)
        data class CodeContentSerializable(
            val collectionName: String,
            val movies: List<SimpleMovieSerializable>
        )

        @JsonClass(generateAdapter = true)
        data class SimpleMovieSerializable(
            val id: Long,
            val title: String,
            val img: String?
        )
    }

    override fun parse(codeContentJsonStr: String): CodeContent? {
        try {
            val c: CodeContentSerializable? = converter.fromJson(codeContentJsonStr)
            return c?.let {
                CodeContent(c.collectionName, c.movies.map {
                    SimpleMovie(it.id, it.title, it.img)
                })
            }
        } catch (e: IOException) {
            throw CodeContentParseException()
        }
    }

    override fun stringify(codeContent: CodeContent): String {
        val c = CodeContentSerializable(
            codeContent.collectionName,
            codeContent.movies.map {
                SimpleMovieSerializable(
                    id = it.id,
                    title = it.title,
                    img = it.img
                )
            })

        return converter.toJson(c)
    }
}

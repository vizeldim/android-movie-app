package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState

abstract class APagingSource<T : Any> : PagingSource<Int, T>() {
    override fun getRefreshKey(state: PagingState<Int, T>): Int? {
        val anchorPos = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPos) ?: return null

        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> {
        val page: Int = params.key ?: 1
        val pageSize: Int = params.loadSize

        val res = getPageData(page)

        val nextKey = if (res.size < pageSize) null else page + 1
        val prevKey = if (page == 1) null else page - 1

        return LoadResult.Page(res, prevKey, nextKey)
    }

    protected abstract suspend fun getPageData(page: Int): List<T>
}